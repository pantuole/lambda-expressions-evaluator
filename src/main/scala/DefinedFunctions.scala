package org.cvut.pantuole

import Parser.parseExpression

import scala.None

object DefinedFunctions {
  private val numeric = "[0-9]+".r

  private def applicationTimes(times: Int): LambdaExpression =
    if (times == 0) {
      Variable("z")
    } else {
      Application(Variable("s"), applicationTimes(times - 1))
    }

  private def intToExpression(num: Int) =
    fnWithArgs(List("s", "z"), applicationTimes(num))

  def fromString(s: String): Option[LambdaExpression] = s match {
    case "+"       => Some(plusLambda)
    case "-"       => Some(minusLambda)
    case "*"       => Some(multiplyLambda)
    case "="       => Some(equalLambda)
    case ">"       => Some(greaterLambda)
    case "<"       => Some(lessLambda)
    case "PRED"    => Some(predLambda)
    case "SUC"     => Some(sucLambda)
    case "ZERO"    => Some(zeroLambda)
    case "DELTA"   => Some(deltaLambda)
    case "T"       => Some(trueLambda)
    case "F"       => Some(falseLambda)
    case "NOT"     => Some(notLambda)
    case "AND"     => Some(andLambda)
    case "OR"      => Some(orLambda)
    case "Y"       => Some(Y)
    case numeric() => Some(intToExpression(s.toInt))
    case _         => None
  }

  // expressions are used from Lambdulus

  private val predLambda = parseExpression(
    "(λ x s z . x (λ f g . g (f s)) (λ g . z) (λ u . u))"
  ).pure

  private val plusLambda = parseExpression("(λ x y s z . x s (y s z))").pure

  private val minusLambda = parseExpression("(λ m n . (n PRED) m)").pure

  private val multiplyLambda = parseExpression("(λ x y s z . x (y s) z)").pure

  private val trueLambda = parseExpression("(λ t f . t)").pure

  private val falseLambda = parseExpression("(λ t f . f)").pure

  private val zeroLambda = parseExpression(
    "(λ n . n (λ x . (λ t f . f)) (λ t f . t))"
  ).pure

  private val deltaLambda = parseExpression("(λ m n . + (- m n) (- n m))").pure

  private val equalLambda = parseExpression("(λ m n . ZERO (DELTA m n))").pure

  private val sucLambda = parseExpression("(λ n s z . s (n s z))").pure

  private val andLambda = parseExpression("(λ x y . x y x)").pure

  private val orLambda = parseExpression("(λ x y . x T y)").pure

  private val notLambda = parseExpression("(λ x t f . x f t)").pure

  private val greaterLambda = parseExpression(
    "(λ m n . NOT (ZERO (- m n)))"
  ).pure

  private val lessLambda = parseExpression("(λ m n . > n m)").pure

  private val Y = parseExpression("(λ f . (λ x . f (x x)) (λ x . f (x x)))").pure
}
