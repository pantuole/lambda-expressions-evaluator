package org.cvut.pantuole

// data type similar to Option, but with description why not
// it's implemented like Monad

sealed trait Maybe[T] {
  def pure: T

  def flatMap[T2](f: T => Maybe[T2]): Maybe[T2]
  def map[T2](f: T => T2): Maybe[T2] = flatMap(x => Yes(f(x)))
}

case class Yes[T](value: T) extends Maybe[T] {
  override def pure: T = value

  override def flatMap[T2](f: T => Maybe[T2]): Maybe[T2] = f(value)
}

case class No[T](why: String = "") extends Maybe[T] {
  override def pure: T = throw new RuntimeException(why)

  override def flatMap[T2](f: T => Maybe[T2]): Maybe[T2] = No(why)
}
