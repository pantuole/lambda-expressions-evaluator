package org.cvut.pantuole

import org.cvut.pantuole.Parser.isSimpleVariable
import scala.annotation.tailrec
import scala.None

@tailrec
def changeVariableName(v: String, except: Set[String]): String = {
  val nextVar = if (v(0) == 'z') "a" else (v(0) + 1).toChar.toString
  if (except(nextVar)) changeVariableName(nextVar, except)
  else nextVar
}

@tailrec
def toUpdatedNames(
    names: Set[String],
    allNames: Set[String],
    acc: List[(String, String)] = List()
): List[(String, String)] = {
  if (names.isEmpty) return acc
  val u = changeVariableName(names.head, allNames)
  toUpdatedNames(names.tail, allNames + u, acc.appended((names.head, u)))
}

def alphaReduce(
    exp: LambdaExpression,
    collisions: Set[String],
    allVars: Set[String]
) = {
  val keyValues = toUpdatedNames(collisions, allVars)
  val updatedNames = Map.from(keyValues)
  alphaReduceHelper(exp, collisions, updatedNames)
}

def alphaReduceHelper(
    exp: LambdaExpression,
    collisions: Set[String],
    updatedNames: Map[String, String]
): LambdaExpression = exp match {
  case Variable(v) =>
    if (collisions(v)) Variable(updatedNames(v))
    else Variable(v)
  case Application(left, right) =>
    Application(
      alphaReduceHelper(left, collisions, updatedNames),
      alphaReduceHelper(right, collisions, updatedNames)
    )
  case Function(a, b) =>
    val nextArg = if (collisions(a.value)) updatedNames(a.value) else a.value
    Function(Variable(nextArg), alphaReduceHelper(b, collisions, updatedNames))
}

def getBoundVars(exp: LambdaExpression): Set[String] = exp match {
  case Variable(value)          => Set.empty
  case Application(left, right) => getBoundVars(left) ++ getBoundVars(right)
  case Function(arg, body)      => getBoundVars(body) + arg.value
}

def getUnboundVars(
    exp: LambdaExpression,
    bound: Set[String] = Set()
): Set[String] =
  exp match {
    case Variable(value) => if (bound(value)) Set.empty else Set(value)
    case Application(left, right) =>
      getUnboundVars(left, bound) ++ getUnboundVars(right, bound)
    case Function(arg, body) => getUnboundVars(body, bound + arg.value)
  }

def betaReduce(
    body: LambdaExpression,
    argumentName: String,
    argumentValue: LambdaExpression
): LambdaExpression = body match {
  case Variable(v) =>
    if (v == argumentName) argumentValue else body
  case Application(left, right) =>
    Application(
      betaReduce(left, argumentName, argumentValue),
      betaReduce(right, argumentName, argumentValue)
    )
  case Function(a, b) =>
    // stop if function argument has the same name
    if (a.value == argumentName) body
    else Function(a, betaReduce(b, argumentName, argumentValue))
}

def evaluateApplication(application: Application): Option[LambdaExpression] =
  application.left match {
    case Variable(value) =>
      DefinedFunctions.fromString(value) match {
        case Some(fn) => Some(Application(fn, application.right))
        case None =>
          Some(
            Application(
              application.left,
              evaluateNormal(application.right)
            )
          )
      }
    case Function(argument, body) =>
      val bound = getBoundVars(application.left)
      val unbound = getUnboundVars(application.right)
      val collisions = unbound.intersect(bound)
      val canBetaReduce =
        collisions.isEmpty || (collisions.size == 1 && collisions.head == argument.value)
      if (canBetaReduce)
        Some(betaReduce(body, argument.value, application.right))
      else {
        val all = bound ++ getUnboundVars(application.left)
        val newFunction = alphaReduce(application.left, collisions, all)
        Some(Application(newFunction, application.right))
      }
    case Application(l, r) =>
      evaluateApplication(Application(l, r))
        .map(exp => Application(exp, application.right))
  }

// reduce left-most application
def evaluateNormal(exp: LambdaExpression): LambdaExpression =
  exp match {
    case Variable(value) =>
      DefinedFunctions.fromString(value) match {
        case Some(fn) => fn
        case None     => exp
      }
    case Application(left, right) =>
      evaluateApplication(Application(left, right)) match {
        case Some(applied) => applied
        case None          => exp
      }
    case Function(argument, body) =>
      Function(argument, evaluateNormal(body))
  }

@tailrec
def evaluateExpression(
    exp: LambdaExpression,
    showSteps: Boolean,
    counter: Int = 0
): Unit = {
  if (showSteps) println("Step " + counter + " " + stringifyExp(exp))
  val nextExp = evaluateNormal(exp)
  if (equalExpressions(nextExp, exp)) {
    if (!showSteps) println(stringifyExp(exp))
    println("Done!")
  } else {
    evaluateExpression(nextExp, showSteps, counter + 1)
  }
}
