package org.cvut.pantuole

import Parser.parseArguments

import scala.annotation.tailrec

case class ParseResult[T](value: T, rest: String)

object Parser {

  @tailrec
  private def parseUntilSpace(
      input: String,
      acc: String = ""
  ): ParseResult[String] = {
    if (input.isEmpty || input.head == ' ') ParseResult(acc, input.tail)
    else parseUntilSpace(input.tail, acc + input.head)
  }

  @tailrec
  private def checkNextChar(
      input: String,
      expectedChar: Char
  ): ParseResult[Boolean] = {
    if (input.isEmpty) return ParseResult(false, input)
    if (input.head == ' ') checkNextChar(input.tail, expectedChar)
    else ParseResult(input.head == expectedChar, input.tail)
  }

  private def isUpperLetter(c: Char) = c.isLetter && c.isUpper

  @tailrec
  def normalizeInput(str: String, acc: String = ""): String = {
    if (str.isEmpty) return acc
    if (str.head == ' ' || isUpperLetter(str.head))
      normalizeInput(str.tail, acc + str.head)
    else normalizeInput(str.tail, acc + " " + str.head)
  }

  @tailrec
  private def extractOneTerm(
      input: String,
      bracketsCount: Int = 0,
      acc: String = ""
  ): ParseResult[String] = {
    if (input.isEmpty || (input.head == ' ' && bracketsCount == 0))
      return ParseResult(acc, input.tail)
    val nextBracketsCount = input.head match {
      case '(' => bracketsCount + 1
      case ')' => bracketsCount - 1
      case _   => bracketsCount
    }
    extractOneTerm(input.tail, nextBracketsCount, acc + input.head)
  }

  @tailrec
  private def extractManyTerms(
      input: String,
      acc: List[String] = List()
  ): ParseResult[List[String]] = {
    if (input.isEmpty) return ParseResult(acc, "")
    if (input.head == ' ') extractManyTerms(input.tail, acc)
    else if (input.last == ' ') extractManyTerms(input.init, acc)
    else {
      val ParseResult(part, rest) = extractOneTerm(input)
      extractManyTerms(rest, acc.appended(part))
    }
  }

  def isSimpleVariable(value: String) = {
    value.length == 1 && value.head.isLetter && value.head.isLower
  }

  private def isValidVariable(value: String) = {
    isSimpleVariable(value) || DefinedFunctions.fromString(value).isDefined
  }

  @tailrec
  private def parseArguments(
      input: String,
      acc: List[String] = List()
  ): Maybe[ParseResult[List[String]]] = {
    if (input.isEmpty) return No("Dot is not found after arguments")
    if (input.head == '.') return Yes(ParseResult(acc, input.tail))
    if (input.head == ' ') parseArguments(input.tail, acc)
    else {
      val ParseResult(argument, restStr) = parseUntilSpace(input)
      if (!isSimpleVariable(argument))
        return No("Argument " + argument + " is not valid")
      parseArguments(restStr, acc.appended(argument))
    }
  }

  @tailrec
  private def parseOneTerm(input: String): Maybe[LambdaExpression] = {
    if (input.isEmpty) return No("Expression is empty")
    if (input.head == ' ') parseOneTerm(input.tail)
    else if (input.last == ' ') parseOneTerm(input.init)
    else {
      val ParseResult(value, rest) = parseUntilSpace(input)
      if (rest.isEmpty && isValidVariable(value)) {
        return Yes(Variable(value))
      }

      if (input.startsWith("(")) {
        if (!input.endsWith(")"))
          return No("Application should end with ). Input: " + input)

        val ParseResult(isFunction, rest) = checkNextChar(input.init.tail, 'λ')
        if (!isFunction) return parseExpression(input.init.tail)
        return for {
          args <- parseArguments(rest)
          body <- parseExpression(args.rest)
        } yield fnWithArgs(args.value, body)
      }

      No("Failed to parse: " + input)
    }
  }

  private def expectAll[T](
      items: List[Maybe[T]],
      acc: List[T] = List()
  ): Maybe[List[T]] = {
    if (items.isEmpty) return Yes(acc)
    items.head.flatMap(h => expectAll(items.tail, acc.appended(h)))
  }

  def parseExpression(input: String): Maybe[LambdaExpression] = {
    val ParseResult(parts, rest) = extractManyTerms(input)
    if (rest.nonEmpty) No("Failed to parse a part" + rest)
    expectAll(parts.map(parseOneTerm)).flatMap(e =>
      e.length match {
        case 0 => No("Empty expression found")
        case _ => Yes(applyMany(e))
      }
    )
  }
}
