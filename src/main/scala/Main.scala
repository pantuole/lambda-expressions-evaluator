package org.cvut.pantuole

import Parser.{normalizeInput, parseExpression}

def readExpressionFromFile(path: os.Path) = {
  os.read.lines(path).reduce((l1, l2) => l1 + l2)
}

@main
def main(): Unit = {
  val showSteps = true;
  val file = os.pwd / "src" / "test" / "resources" / "simple.lambda"
  val expression = readExpressionFromFile(file)

  parseExpression(normalizeInput(expression)) match {
    case Yes(exp) => evaluateExpression(exp, showSteps)
    case No(why)  => println("Error occurred while parsing expression - " + why)
  }
}
