package org.cvut.pantuole

sealed trait LambdaExpression

case class Variable(value: String) extends LambdaExpression

case class Application(left: LambdaExpression, right: LambdaExpression)
    extends LambdaExpression

case class Function(argument: Variable, body: LambdaExpression)
    extends LambdaExpression

def equalExpressions(exp1: LambdaExpression, exp2: LambdaExpression): Boolean =
  exp1 match {
    case Application(l1, r1) =>
      exp2 match {
        case Application(l2, r2) =>
          equalExpressions(l1, l2) && equalExpressions(r1, r2)
        case _ => false
      }
    case Function(a1, b1) =>
      exp2 match {
        case Function(a2, b2) =>
          equalExpressions(a1, a2) && equalExpressions(b1, b2)
        case _ => false
      }
    case Variable(v1) =>
      exp2 match {
        case Variable(v2) => v1 == v2
        case _            => false
      }
  }

def toVars(names: String*) = names.map(n => Variable(n)).toList

def fnWithArgs(names: List[String], body: LambdaExpression): LambdaExpression =
  if (names.isEmpty) body
  else Function(Variable(names.head), fnWithArgs(names.tail, body))

def applyMany(expressions: List[LambdaExpression]): LambdaExpression =
  if (expressions.size == 1) expressions.head
  else if (expressions.size == 2) Application(expressions.head, expressions(1))
  else Application(applyMany(expressions.init), expressions.last)

def isApplication(expression: LambdaExpression): Boolean =
  expression.isInstanceOf[Application]

def isFunction(expression: LambdaExpression): Boolean =
  expression.isInstanceOf[Function]

def stringifyExp(exp: LambdaExpression, inFn: Boolean = false): String =
  exp match {
    case Variable(value) => value
    case Application(left, right) =>
      if (isApplication(right)) {
        stringifyExp(left) + " (" + stringifyExp(right) + ")"
      } else {
        stringifyExp(left) + " " + stringifyExp(right)
      }
    case Function(arg, body) =>
      val separator = if (isFunction(body)) "" else " . "
      val start = if (inFn) " " else "(λ "
      val end = if (inFn) "" else ")"
      start + arg.value + separator + stringifyExp(body, true) + end
  }
