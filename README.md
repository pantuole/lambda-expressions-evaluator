# Evaluator of λ-expressions

## Project Description

I implemented an evaluator of λ-expressions in Scala. The evaluator supports reading the λ-expressions from the file and their step by step evaluation based on the normal evaluation order.

## Evaluation

- ONLY Normal evaluation order (for now) using beta and alpha reductions
- Step-by-step evaluation with full expansion of integers, literals
- All defined functions like ZERO, NOT, PRED ... should be upper case 
- (in a case it get "zero", it will be evaluated as "z e r o" like it's done in Lambdulus)
- Supported functions: +, -, *, =, >, <, PRED, SUC, ZERO, DELTA, T, F, NOT, AND, OR, Y, positive integers


## How to run
- Install sbt [https://www.scala-sbt.org/download.html] 
- Run using "sbt run" command

### Configuration (in Main.scala file)
- Change the variable "path" to change the lambda expression path
- Change the variable "showSteps" to false if you need only the resulting lambda expression