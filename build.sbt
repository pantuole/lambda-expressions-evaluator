ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "3.1.3"

lazy val root = (project in file("."))
  .settings(
    name := "semester-project",
    idePackagePrefix := Some("org.cvut.pantuole"),
    libraryDependencies += "org.scala-lang" %% "toolkit" % "0.2.0"
  )
